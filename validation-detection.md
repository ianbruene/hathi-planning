# Validation and detection of entities within messages

## Validation of an incomming object

The Context-type header MUST contain either 'application/ld+json; profile="https://www.w3.org/ns/activitystreams"', "application/activity+json". It SHOULD be the former.

The @context field SHOULD be present and if present MUST contain "https://www.w3.org/ns/activitystreams", the "https" MAY be replaced with "http". This can be either as the only value in the field, one of the values if the field is a list, or in the @vocab field in an object.

The @context field MAY have a @language field, containing the language code for the default language for the packet.

The @context MAY be or contain objects with fields specifying aliases for extensions to the specification. The field name is the alias and the value is the address of the extension specification.

A @type or type field MUST exist, it SHOULD be the former. The field MAY be a list.

All bare fields MUST be defined within the ActivityStreams or ActivityPub specification. Field names in the format "source:fieldName" are used for fields defined in extensions. The source "as" is a builtin alias for the ActivityStreams specification, so "foo" and "as:foo" are identical.

If this object is being received as the response to a GET request or as a server to server POST request, it MUST contain an @id or id field. It SHOULD be @id.

If this object is being received as a client to server POST request it SHOULD NOT contain an @id or id field. If present it will be ignored.

## Proper detection and categorization of objects embedded within other objects

If an embedded object does not have a @type or type (@type prefered) then it is a complex datatype. Language maps are a good example.

If an embedded object has a @type/type it is an object of three possible types. Two of those can be distinguished from each other, but the third can look like either of the others. This ambiguity is solved by the handlers for each Activity type claiming the embedded objects that are relevant to them (for example the contents of the object field in a Create); after this the remaining objects will be either a reference with additional metadata, or a transient object.

A reference+metadata has an @id/id field. A transient object does not.
