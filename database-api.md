# Database API

This does not include the functions for setting up, opening, or closing a database. Only the functions used for dealing with data are included.

Most of these functions take a pointer to a transaction so that related interactions with the database can be properly grouped for performance, and in case of error, reversion. If the transaction pointer == nil the function will setup its own one-off transaction. This is to make the simpler use cases of the API easy for the calling code, without also proliferating the API with transaction and non-transaction versions of each function.

## Objects

* GetObject(tx, objectID HathiID) (*Object, error)

Pulls all the data for an object, and returns it.

* GetObjectCore(tx, objectID HathiID) (*Object, error)

Pulls the core fields for an object and returns them.

* SetObject(tx, obj *Object) (error)

Stores the provided object in the database. If there is already an object in the database with that ID it is overwritten.

* DeleteObject(tx, objectID) (error)

Removed the object from the database. Use not recommended as tombstones should be left.

## Miscellaneous

* GetAllObjectIDs(tx) ([]HathiID, error)

Retrieves the IDs of all objects that are present in the database and returns the list.

* GetAllPrivilegeIDs(tx) ([]HathiID, error)

Retrieves the IDs of all objects privileges that are present in the database and returns the list.

* BeginTransaction() (tx, error)

Begins a transaction, returning a pointer that can be passed to other functions.

* EndTransaction(tx, *error) (error)

Ends a transaction. Takes a pointer to error so that it can be used with the defer command to either commit or revert the current transaction. If the error pointer in nil it commits.

## Privilege

* CheckPrivilege(tx, objectID HathiID, actorID HathiID, bitName string) (bool, error)

Returns the status of the privilege bit bitName for actorID on objectID.

* GetObjectPrivileges(tx, objectID HathiID) (map[HathiID][]string, error)

Returns all of the privileges associated with the requested object. Map keys are actor IDs, string lists are the bits

* GetObjActPrivileges(tx, objectID HathiID, actorID HathiID) ([]string, error)

Returns a list of all of the privileges associated with the specified Actor on this Object.

* SetObjectPrivileges(tx, objectID HathiID, actors map[HathiID][]string) (error)

Sets all privileges for the Object. If any privileges exist they will be overwritten.

* SetObjActPrivileges(tx, objectID HathiID, actorID HathiID, privilegeBits []string) (error)

Sets all privileges for the Actor on this Object.

* SetPrivilege(tx, objectID HathiID, actorID HathiID, bitName string, value bool) (error)

Sets or clears a specific privilege bit for the given Actor and Object.

## Items

Note: This section is the current setup. There is a very high likelyhood of change during the format refactor.

* ItemsGetI(tx, objectID HathiID, index int) (entry HathiID, err error)

Returns the value of Object.items[index].

* ItemsGetRange(tx, objectID HathiID, start int, range int) ([]entries HathiID, err error)

Returns the values in the range start:start+range. It properly handles negative ranges, and a range of 0 means unbounded.

* ItemsAppend(tx, objectID HathiID, entry HathiID) (err error)

Adds the value to the end of the list.

* ItemsAddI(tx, objectID HathiID, index int, entry HathiID) (err error)

Inserts the value at the specified index, moving existing items out of the way.

* ItemsRM(tx, objectID HathiID, index int) (err error)

Removes the value at the index.

* ItemsEdit(tx, objectID HathiID, index int, entry HathiID) (err error)

Sets the value of items[index] to the new value.

* ItemsMaxIndex(tx, objectID HathiID) (index int, err error)

Returns the highest index of the list.
