# Hathi Planning Repository
Here you will find documents and overviews related to the planning of Hathi.

All planning documents have formerly been found in the [wiki](https://gitlab.com/hathi-social/planning/wikis/home), 
but everything have now been moved down into the directory for editing convenience.

Contributions are welcome, most especially in the rfc-documents in /discussions;
Getting a feel for things through the #hathi channels on either Discord or IRC is encouraged, but to make a formal suggestion,

1. Clone the repository
2. Edit the relevant file/section
3. Create a merge request

Discussions take place mainly in Discord channels created for the
purpose in the Hathi section of ICEI's server;
Discussion documents serve as summaries and overviews of both ongoing
discussions and finalized decisions.
