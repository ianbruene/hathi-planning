
# Hathi intermodule API


## Object Server

* PostToOutbox(sessionID, actorID, object)

Posts the object to the outbox of the specified actor if the sessionID is
valid for that actor.

* PostToInbox(actorID, object)

Posts the object to the specified actor's inbox. Server to server only.
Inter-server auth?

* Privilege functions...

Used for granting/revoking privileges to an object for actors who do not own
it. Unwritten, not needed before MVP.

* CreateActor(actorID)

Used by identity manager only. Inter-module auth?

* DeleteActor(actorID)

Used by identity manager only. Inter-module auth?


## Identity Manager

Identities need some sort of GUID so they can be refered to without the key
itself.

* CreateIdentity()

Unknown arguments

* DeleteIdentity(sessionID)

* CreateActor(sessionID, actorID)

* DeleteActor(sessionID, actorID)

* AddIdentityToActorOwners(sessionID, actorID, identityID)

* RemoveIdentityFromActorOwners(sessionID, actorID, identityID)

* IsSessionValid(sessionID, actorIDList)

Returns true if the sessionID is valid for any of the actors in the list.


## Router

This just repeats some of the other functions. Leaving blank for now.
