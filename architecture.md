# What modules exist and what they do

## Object Server

The object server handles the hathi objects that make up the contents of the
network. This includes storage, propagation to other parts of the network, and
side effects. The object server is the core which the other modules are built
to support.

## Identity and Session Manager

The identity manager handles user login / authentication, and authorization.
The client normally only talks to the identity manager once per session to
authenticate. After that the per-action authorization is handled by the object
server asking the identity manager for verification that the current session
ID is valid. Creating and deleting identities and actors is handled by this
module.

## Router

The router exists to provide a single interface for the client to talk to. It
them routes messages to the other modules as needed. The router only exposes
messages that external programs are allowed to use.

## Protocol bridge (optional, not for MVP)

A protocol bridge would exist to translate between the Hathi protocol and
other compatable protocols, for example the ActivityPub over HTTP system. In
theory this module could be combined with a router.


# How modules communicate

Modules communicate over a custom protocol designed to behave as remote
proceedure calls. For this reason most of the API documentation will be
written as if they are functions.


# What objects are

Objects are JSON strings as defined in the ActivityPub/ActivityStreams
specifications. Objects divide into Actors, Activities, and everything else.
Actors roughly corrsepond to user profiles. Activities represent actions
taken on the network, and the rest contain or link to various types of data
traveling on the network.


# How objects are identified

Objects are identified through their "@id" field, which is always a string.
While the exact format of the ID is not yet certain, it will likely be in a
format something like "serverName/actorID/objectID". The actor ID contained
in an object's full ID is what determines the ownership of that object.

An exception to this rule exists for Actors, which drop the objectID part of
the format.


# How objects move across the network

An object enters the network when a client posts it to an Actor's outbox. If
the client is validated for that Actor the server will perform any side
effects that are relevant to the type of object. The server will then send
the object to the inbox of any Actors that are effected by the object, for
example if that Actor is listed in the object's "to" field. This mechanism
serves to propogate messages both around a single server as well as between
servers.


# What effects objects have

Many objects have side effects depending on their type. Most generally objects
containing fields such as "to", "cc", "inReplyTo", etc. will have the side
effect of being propogated to a specified destination.

Examples specific to certain object types include:

* An Accept issued in reply to a Follow will cause Actors to add each other to
their following/follower lists.

* A Delete, if targeted at an object the user is allowed to change will remove
the object and replace it with a Tombstone.


# General features of the intermodule api

Most functions in the API carry with them a session ID which is used to
authorize the action. If the session ID is not provided (or equals nil) the
command is treated as authenticated by guest; useful for retrieving a publicly
accessible object. Multiple commands can be batched in a single packet.


# What an identity is

An identity can be thought of as the keys that a user authenticates to the
server with. The Identity Manager maintains a mapping between keys and the
Actors they own.


# What an actor is

Strictly speaking, an Actor is just another object in the object server. In
usage an Actor is the conduit through which users interact with the network.
Some Actor types are meant for use by individuals (much like a normal social
network), others for organizations or bots. The Group Actor takes the place
of mailing lists and other types of group that may be desired.


# How identities relate to actors

Identities answer the question of "may I post to this Actor's outbox?". A
single identity may control multiple Actors, and an Actor may have several
identities which are allowed to control it.


# Actor permissions

Actors may do anything to the objects that they directly own (as determined by
object ID). An Actor may also allow other Actors to modify objects it owns.
This system is seperate and unrelated to the identity system. Object
permissions are tracked and enforced by the object server.


# Different types of client

* "Client" / "User client" / "We need a better name for this"

Client intended for use by an individual. Analogous to an email client, or the
apps of other social networks

* "Group client" / "Group daemon"

Client designed to run a Group Actor. Stradles the line between server and
client.

* "Service daemon"

Daemon for running bots.
