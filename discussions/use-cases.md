# User features

## Overview

Hathi is designed to be a decentralized communication platform in the same spirit that USENET used to provide (see [problem statement](../problem-statement.md) and [mission statement](../mission-statement.md)). 

The following are use cases of Hathi.

## Create a profile

A user should be able to create an account on a Hathi instance. The user should 
be expected to provide the following information:

* Some form of authentication credential for future use such as a username and password.
* A display name

The Hathi instance should store the salted hash of the password with the username and used to validate the user at a later time. The instance should also have a mechanism to verify the identity of the user creating the accoun. The following options are to be considered:

* Send an email with a confirmation code, to be entered by the user to the Hathi instance
* OAuth
* GPG key

Once the Hathi instance has verified the user's identity, it should provision the necessary collections as specified in [ActivityPub 5. Collections](https://www.w3.org/TR/activitypub/#collections). The user will then be able to login to the Hathi instance using the correct credentials; and the Hathi instance provide other functionality available to an existing user account.

## Login to Hathi

A user should be able to enter the authentication credential provided when creating the account (see "Create an account" above) to login to their account on the Hathi instance. When the credential has been validated, the user is notified and the user is able to access functionality available to authenticated users.

## Update display name

An authenticated user should be able to update the display name of their account. The user enters the new value through an interface and submits the new value. The Hathi instance should store that information and use that for cases where the user's display name is needed.

## Update their login password

An authenticated user should be able to update their password. The user should provide their existing password and the new password. When the Hathi instance has validated that the provided existing password is correct it should replace the currently-stored salted hash with a salted hash of the new password.

At this point, the user should be expected to use the new password.

## View an instance user's profile

Any user should be able to view another user's profile information using an ActivityPub-compliant UI.

## Request to follow a user

An authenticated user should be able to follow another user ([ActivityPub 7.5 Follow Activity](https://www.w3.org/TR/activitypub/#follow-activity-inbox)); the other user can be on another ActivityPub instance.

If the user to follow is on the same instance, the user should be able to send the follow request while viewing the Hathi user's profile. If the user to follow is on another ActivityPub instance, the user should be able to enter the other person's ID.

## Disposition a follow request

An authenticated user should be able to accept ([ActivityPub 7.6 Accept Activity](https://www.w3.org/TR/activitypub/#accept-activity-inbox)) or reject ([ActivityPub 7.7 Reject Activity](https://www.w3.org/TR/activitypub/#reject-activity-inbox)) another user's follow request.

## Block a user

An authenticated user should be able to block another user either on the same instance, or another ActivityPub instance, as specified in [ActivityPub 6.9 Block Activity](https://www.w3.org/TR/activitypub/#block-activity-outbox).

If the user to block is on the same Hathi instance, the user should be able to block using the other user's ID.

## View followers

An authenticated user should be able to view their followers list. From that list, the user should be able to navigate and get more details for anyone on that list.

## View who they are following

An authenticated user should be able to view their following list. From that list, the user should be able to navigate and get more details for anyone on that list.

## Create a note

An authenticated user should be able to create a note. The user should be able to decide the note's recepient as specified in [Activity Stream Section 5.1 Audience Targetting](https://www.w3.org/TR/activitystreams-vocabulary/#audienceTargeting).

If any of the receipients are in another ActivityPub instance, the Hathi instance should send the note according to [ActivityPub 7.2 Create Activity](https://www.w3.org/TR/activitypub/#create-activity-inbox).

## Get a list of notes they wrote

An authenticated user should be able to retrieve a list of notes that they have written.

## Edit a note

An authenticated user should be able to edit a note that they have previously created.

## Get a list of notes they've received

An authenticated user should be able to retrieve a list of notes they have received. These notes can be sent explicitly to them; or were sent public by a user they are following.

## View a note

A user should be able to view notes that they have been given access to by the note's creator. An authenticated user should also be able to view a note that they created on the Hathi instance.

If the note is in reply to another note, a reference to the note should be displayed.

If the note has replies, a reference to the replies should be displayed.

## Reply to a note

An authenticatee user should be able to reply to a note that they are able to view. When the Hathi instance should include the [inReplyTo property](https://www.w3.org/TR/activitystreams-vocabulary/#dfn-inreplyto) with the ID of the note being replied to as its value in the Note object.

## Like a note

An authenticated user should be able to "like" a note. The Hathi instance should add the note's ID to the user's "liked" collection (See [ActivityPub 6.8 Like Activity](https://www.w3.org/TR/activitypub/#like-activity-outbox)).

If the note being liked is in the same instance as the user, the Hathi instance should add the user's ID to the Note's "liked" collection (See [ActivityPub 7.10 Like Activity](https://www.w3.org/TR/activitypub/#like-activity-inbox)). If the note is on another ActivityPub instance, the Hathi instance should send the Like Activity to the note creator's inbox if the "attributedTo" property is present on the note.

## View a list of liked notes

An authenticated user should be able to view a list of notes that they have liked. From that list, the user should be able to view the ful note.

## Record a remote user "liking" a note

When another ActivityPub instance sends an HTTP POST of a [Like Activity](https://www.w3.org/TR/activitypub/#like-activity-inbox) on an existing note ID, the Hathi instance should add the remote user's ID to the note's "likes" collection. The Hathi instance should ignore the Like Activity if the "content" property is not an ID.

## View a count of how many times a Note has been liked

A user viewing a note should be able to see how many times a note has been liked.

## Get a list of users who have liked a note

A user should be able to see a list of users who have liked a note. Users from both the same Hathi instance, and other ActivityPub instances, should be displayed. The user should also be able to view a user's profile from that list.

## View a threaded note list

A user should be able to view the list of notes in a thread view.