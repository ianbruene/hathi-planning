Identity handling
---------------------

-----------------------------------------------------------------------

### Overview

Identity handling is the validation, storage, retrieval, and migration
of identities and their related data.

The scope of this includes but is not necessarily limited to:

- Validating that the Actor is who she/he/it claims to be
- Retrieving relevant authorizations for this Actor
- Storage and retrieval of data relevant for this Actor
- Migration of Actor data between servers
- Ensuring discoverability of Actors even if their former server  
  went down, or if said server actively attempts to misdirect  
  the ones contacting it

Also, we are attempting to solve [Zooke's Triangle](https://en.wikipedia.org/wiki/Zooko%27s_triangle) here.  
It is not going to easy, and probably won't end up completely perfect
either.  

-----------------------------------------------------------------------

### Definitions

- Authentication is the act of confirming the truth of an attribute of a datum or entity
- Authorization is about what you are permitted to do

-----------------------------------------------------------------------

### Requirements

- User import/export feature
  - MUST include
    - account info (exported-from UUID, avatar, description, etc.)
    - following list
    - quiet/silence/block lists
    - export timestamp
  - MAY include
    - user’s post history
    - other settings

-----------------------------------------------------------------------

### Potential Platforms

-----------------------------------------------------------------------

#### OAuth/OpenID

OAuth is an authorization protocol, and OpenID is an authentication
protocol built on top of OAuth, with external 'identity providers'
handling the login and user management end of things, while also
providing the identity's information to authorized third parties.

##### Pros
The attraction of OAuth and OpenID is that they're open source and
widely adopted.

##### Cons
The downside of these protocols is that they depend on external identity
providers.

- They are open to denial of service attacks
- Many users will use their facebook accounts to log in, exposing  
  private information to potentially malicious Hathi servers
- If we rely on third party identity servers, we cannot rely on the  
  identity information provided
- For proper control of user data, we would need to implement our own  
  identity provider
- Like Hathi servers, uptime of any given individual identity provider  
  cannot be guaranteed

References:  
[why-oauth-it-self-is-not-authentication](http://blog.api-security.org/2013/02/why-oauth-it-self-is-not-authentication.html)  
[difference-between-oauth-openid-and-openid-connect-in-very-simple-term](https://security.stackexchange.com/questions/44611/difference-between-oauth-openid-and-openid-connect-in-very-simple-term/130411#130411)  
[whats-the-difference-between-openid-and-oauth](https://stackoverflow.com/questions/1087031/whats-the-difference-between-openid-and-oauth)  
[oauth-authorization-vs-authentication](https://stackoverflow.com/questions/33702826/oauth-authorization-vs-authentication/33704657#33704657) (Has some explanation of history of purposes behind the different protocols)  
[oauth-openid-youre-barking-up-the-wrong-tree-if-you-think-theyre-the-same-thing](http://softwareas.com/oauth-openid-youre-barking-up-the-wrong-tree-if-you-think-theyre-the-same-thing/)  

-----------------------------------------------------------------------

#### Zot Protocol / HubZilla Nomadic Identity

Update: [Zot technical description and spec](https://project.hubzilla.org/help/en/developer/zot_protocol)

HubZilla is the only one implementing this.  
But.  
In essence their nomadic identity is implemented by having a global id, secured against collisions with a high bit number, authenticated through public/private key pairs, with the servers merely being 'the place where that user is at right now'.  
This would solve our problem of migrating between servers where the 'from' server is down or malicious.  

HubZilla apparently has a working implementation, and it can interoperate with ActivityPub.  
It does seem to break spec, though.  
> To implement a proper nomadic identity system, a given software would have to generate a globally unique ID for every single profile. Zot (the protocol Hubzilla uses) generates a 256-bit GUID, for which the expected collision time is around a million longer than the existence of the entire universe.

> Zot links this GUID to a public key, and nomadic identity is accomplished by cloning data across each server, so that each URI is dereferenced to the GUID. By comparison, ActivityPub is also supposed to be dereferencable (so each URI should point to an object).

> The problem comes in implementing the id parameter. Whereas Zot has a GUID that is authoritative over the URI (meaning GUID is used internally and URI is only for lookup), ActivityPub uses the URI as the ID (meaning each AP implementation must implement its own UID system internally).

##### Pros
- There is an existing implementation
- It seems to work well
- And it can do everything we need
- Interoperability with HubZilla may not be a bad thing; it seems more productivity/work oriented than other decentralized social networks

##### Cons
- Spec expects id to be based on server
- Fork in the road: Doing identity this way will affect architecture, interoperability and user base in a major way
  - Nb: HubZilla *is*, apparently, able to run over ActivityPub protocol

Source: [Investigate Nomadic Identity](https://github.com/pixelfed/pixelfed/issues/216)

Other sources:  
[About HubZilla 3.0](https://medium.com/we-distribute/the-do-everything-system-an-in-depth-review-of-hubzilla-3-0-692204177d4e)  
[About Nomadic Identity](https://medium.com/@tamanning/nomadic-identity-brought-to-you-by-hubzilla-67eadce13c3b)  
[HubZilla/Nomadic Identity Howto](https://medium.com/@tamanning/getting-started-with-nomadic-identity-how-to-create-a-personal-channel-on-hubzilla-7d9666a428b)  
[HubZilla History](http://www.talkplus.org/blog/2016/the-history-of-hubzilla/)  
[Mastodon discussion on possibly using HubZilla's identity system](https://discourse.joinmastodon.org/t/federation-of-user-accounts-and-follows-followers-across-activitypub-services/996)  

-----------------------------------------------------------------------

#### Address service implemented as Service Actor

##### Pros
- User/actor can select x address services to inform others about their server-wise whereabouts.
- Server dependency can be avoided

##### Cons
- We'd have to be running an address service, at least until the network reaches self-sustainability
  - This kind of required work holds true for whichever identity management system we choose to implement though
- Do we want to take the responsibility of possibly adding an extension to ActivityPub?
  At some point we will interact with other ActivityPub instances that may want to integrate with this address service
  At that point we have to manage the protocol
  - This kind of required work holds true for whichever identity management system we choose to implement though

-----------------------------------------------------------------------

#### Publishing identity based on crypto/blockchain implementation

##### Pros
- Decentralized  
  Very hard to create denial-of-service attacks this way
- De-monopolization of identity data
  Divorces server control from identity, which is great for consumers

##### Cons
- Exclusion  
  Risk of excluding the developing world and a few other places with too high requirements
- Usability  
  There's a danger of locking naive users out of their own identities in an unrecoverable way due to them not knowing how to manage keys well
  - Who is our target audience? And can they be trained to handle this?
- Complicated design  
  If we use crypto, but don't wrap identity in with whatever else we're doing (i.e. if we need seperate crypto for deliverability, etc),  
  we've created an architectural mess that everyone will pay for in the long run

-----------------------------------------------------------------------

### Suggestions

- Issue tokens for clients so we don’t have the password merry-go-round
- We should have some way to self-certify account migrations and alternate accounts via GPG or some other common credential.
