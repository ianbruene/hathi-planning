
Proposal: Implement Hathi as separate app running on secure scuttlebot
-----------------------------------------------------------------------

### Details

Make Hathi as an app running on top of Scuttlebot and its gossip
protocol as transport layer.  
The Patchwork app follows a timeline design like facebook.
We don't want a timeline design.  
We want a forum-like design with a good overview of whatever themes and
discussions we choose to follow, not whatever the most recent activity
happens to be.  

Implement private groups as a Scuttlebot identity, following the 
ActivityPub model for communication between user clients and group
clients, using the Scuttlebot network as server.  

Have groups self-identify by listing taxonomy tags and let the Hathi
app sort all groups subscribed to into the various categories.

-----------------------------------------------------------------------

### Strength

Design has built-in identity management and built-in encryption.

Scuttlebot is already in existence, it has similar goals to Hathi,
and it has an existing, friendly and collaborating user base.

-----------------------------------------------------------------------

### Weakness

The number-crunching going on to encrypt and decrypt the gossipping is
slow and ressourceconsuming.  
Arguably this is an advantage in that it stops people from 'chatting'.  

If the decryption can be optimized it would be good; if it cannot,
Scuttlebot might have a scaling problem whenever the number of
connections (people/hubs that one is connected to) gets too high.  

Also: Crypto is hardcoded.

-----------------------------------------------------------------------

### Opportunity

Scuttlebots user base is active and there are independent initiatives
on the developing front.  
It is also a user base that is interested in alternative platforms, and
they share their knowledge about them with each other.  
In other words, if our cause appeals to them.. which it should, given
their interests.. we could pick up volunteers by getting involved in
this way.  

Crypto is hardcoded. But also, they are interested in doing something
about that. A review on the security of their encryption would most
likely be appreciated.  

Code base is apperently somewhat jumbled.  
However, there's an initiative starting, main goals being to document
and reimplement the existing code base in order to pave the path for
new developers.  
They might appreciate collaboration. It might not exactly be a 'rescue'
operation, but it seems to me [UW] that it would call for the same
mindset and techniques.

We might be able to leverage Hathi-server to serve as Scuttlebot hub.  
Especially if our server will be more ressource-efficient than the existing
hubs.

-----------------------------------------------------------------------

### Threat

Crypto is hardcoded.  
If nothing happens on this front, communications will probably not be
all that secure.  

Groups. There is already some thoughts floating about on how to
implement group communication, including private communication.  
This use case might be covered by others in this way, and Hathi not
catch on.  
Actually, our specific use case of security researchers sharing
information might already be covered by a recent pull request by the
main developer.  
However, our main focus of a communications and collaborations platform
is still missing and it is unlikely that other groups will work on such
a thing in a focused manner.  

Hubs. It's apparently difficult to install the Scuttlebot hubs, or maybe
they require a lot of resources.  
There are not that many of them, not enough to safeguard against
DOS-attacks.  

-----------------------------------------------------------------------
