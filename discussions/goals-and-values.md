-----------------------------------------------------------------------

# Goals & Values

-----------------------------------------------------------------------

## TLDR Summary

-----------------------------------------------------------------------

Values in bold, with associated goals below:

* **Freedom of Choice**
  * Support free movement of users and profiles between servers
  * Have systems be maximally configurable
* **Modularity**
  * Each network component should "Do one thing and do it well"
* **Efficiency**
  * Only use the resources necessary to accomplish the users' required function

-----------------------------------------------------------------------

## Reasoning

-----------------------------------------------------------------------

At the moment, countless of attempts at competitive social platforms are
being written. To fulfill our mission, we need to either join forces
with one of these, or be better at them at fulfilling our mission goals.

Currently, every platform, including the upcoming decentralized 
platforms, fails in fulfilling our mission goals in one of the following 
ways:

* Lack of privacy  
  Access to user data, lack of or badly implemented encryption, or
  simply undefined data handling procedures.
* Bloat  
  More features than the user needs, slowing them down.  
  Too many notices distracting the user from their task.  
  Too many background processes trying to guess what the user wants, 
  effectively being a resource sink and a distraction.
* Centralized control  
  Whether due to the software being developed by a private company or
  the organization behind the social platform ignoring its users for
  reasons unknown, the problem here is that the cost in leaving the
  platform is too high.

Here's the thing.  
All technically knowledgeable users, and a lot of mainstream users, are
inherently aware of these things already.  
They will look at any emerging platform with a critical eye.  
The question foremost in their minds are, "Is this one any different?"  

In order to retain interest and relevance as one project among many
in an explosively growing ecology of countless alternatives, we need to
delineate extremely sharply that we are perfect for our niche.

We need to be able to make principled statements, to argue for them, and
to clearly refute any remaining doubts.

-----------------------------------------------------------------------

### Freedom of Choice

Freedom of Choice is what is currently lacking from every major social 
platform out there.  
It is a thing that we need to get right.  
If we try to get it right without having a plan, we will fail.  

Hackers and security-conscious developers are our prime target as users;
if we fail here, or even 'only almost succeed', we have no relevance to 
our prime userbase, most likely resulting in no relevance whatsoever.

-----------------------------------------------------------------------

### Modularity

"Do one thing and do it well" is a well-known principle of the open 
source movement and this is for good reason.  
It is what allows the complexity that is Linux to continue to reinvent
itself while still being able to draw on the rich ecosystem of system
components underlying the architecture.  

Hathi, too, is complex. ActivityPub is complex. The 15+ competing and  
collaborating projects in the decentralized social sphere based on
ActivityPub are not going to make things simpler.  

A big project can allow itself to err and become internally
interdependent; its sheer momentum of traffic, users and developers will
allow it to roll on.  

Small projects need to provide services essential to the other parts of
the ecosystem, or be so light on developer resources than they do not
really care what it is or isn't used for.

We do not really know whether we are or will be a big or a small
project.

But it seems clear that in the overall scheme of things, it will be 
better to design to be as interoperable as possible in order to make
use of other networks' features - as long as interoperability does not
interfere with core values or core features.

Interoperability is potentially a very, very powerful thing.  
There is nothing in particular that stops one service from using 12
other services as input. And unlike the web, noone has to obfuscate
their content with graphics, typography or helper scripts if they do not
wish to.

Paraphrasing three quotes from Doug McIlroy,  

> Make each ~~program~~ *network component* do one thing well. To do a new job, build afresh 
rather than complicate ~~old programs~~ *existing components* by adding new features.

> Expect the output of every ~~program~~ *network component* to become the input to another, as yet unknown, ~~program~~ *network component*. Don't clutter output with extraneous information. Avoid stringently columnar or binary input formats. Don't insist on interactive input.

> Write programs to work together. Write programs to handle ~~text~~ *json* streams, because that is a universal interface.

And reiterating the Rule of Least Surprise,

> Rule of Least Surprise: In interface design, always do the least surprising thing.

-----------------------------------------------------------------------

### Efficiency

The ideal of low resource consumption is something that we have chosen
as a goal for the project both because most other solutions are so very
full of bloat but also, and more strategically relevant, in order to
be able to support developing nations and other areas of slow and/or
intermittent network access, making us able to spread in demographic 
areas whose needs are not met by other solutions - and thereby also to
the nascent developer groups in those areas.

-----------------------------------------------------------------------

## Inspiration & further reading

-----------------------------------------------------------------------

[Basics of the Unix Philosophy](http://www.faqs.org/docs/artu/ch01s06.html)

-----------------------------------------------------------------------
