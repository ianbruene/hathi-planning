General-purpose propagation layer protocol for automatic, secure encryption
---------------------------------------------------------------------------

The idea here is to us an alternative data transportation layer for the Hathi network instead of https, in order to make the propagation more secure.  
The original idea for implementation was FrameFlood, but as it turned out there were issues with it which were not easily overcome.  
..We still have no final conclusions on this idea; the decision at the time was to go forward with Hathi rather than postpone too much; but thinking in terms of alternative transport layers would still be a good idea.
