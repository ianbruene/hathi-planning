Content filtering
------------------

### Ideas

- A mode that prioritizes content based on per-user trust metrics.

- Groups
  - Trust networks (e.g. advogato algorithm, UW should find material/links)
  - Everyone connected in 'distance' graph with default values, users can set minimum threshold for allowed contact
