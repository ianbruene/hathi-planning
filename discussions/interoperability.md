Interoperability
-------------------

-----------------------------------------------------------------------

ActivityPub lends itself to interoperability, although.. it is also 
perhaps so loosely defined that interoperability with others is not 
always feasible.

Our prioritization of interoperability will depend to a large extent 
on our own number of users vs other AP network's numbers of users, 
but it is still a nice thing to strive for.

-----------------------------------------------------------------------

#### Ideas

- Ostatus bridge to allow federation with ostatus-based server instances
