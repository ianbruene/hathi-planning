Delivery scopes
---------------

- Public  
  - shows up in local timelines
  - shows up in federated timelines
  - shows to followers
  - shows to anyone who sees a repeat.
- Local (previously named local+)
  - shows up in local instance timeline
  - shows to followers
  - shows to those who see repeats
  - does  NOT show in federated timelines
- Unlisted
  - only shown to those who directly follow or see a repeat
- Message (previously named whisper)
  - not guaranteed to be private, especially when passing
    between instances
  - is less-public
  - useful for a quick “here’s my email/phone/etc info to follow up”.
- Group
  - deliver only to a specific group
  - actually covered by doing Message to a group Actor
