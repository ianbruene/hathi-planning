The code currently uses RFC3339 timestamps and duration strings.

The reason for this is that RFC3339 is prefered in the absense of anything that directly contradicts it. ActivityPub/ActivityStreams stipulate ISO8601 formatting, which is an incompatable superset of RFC3339. JSON-LD (which AP/AS are built on top of) on the other hand prefers RFC3339.

RFC3339 is easier to implement. It is also likely that we will not be adhering strictly to ActivityPub due to whatever authentication system we adopt. Therefore I have decided to use RFC3339 formatting unless something forces this in the other direction.
