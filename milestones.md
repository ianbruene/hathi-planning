* **Progress**
  1. [x] 0.1 Prototype
  1. [ ] 0.2 Minimum viable product
  1. [ ] 0.3 Preparation for GUI client development
  1. [ ] 0.5 Getting ready to stabilize client API
  1. [ ] 0.7 Test in the wild; last major changes pre-1.0
  1. [ ] 1.0 Stable framework and server API
  1. [ ] Future: Maybe/Someday/I-Want-a-Unicorn-Dammit


##### 0.1 Server prototyping (Done)
* Questions for the prototype to answer
  * Will Go work well as an implementation language for the server?
  * Yes, Go seems like a good fit for the purpose.
* Establish a testable framework
* Work gradually towards a stable code structure

##### 0.2 Minimum viable product
* Discussion
   * Choice between sqlite and postgresql back-end.
   * Do we want for users to be able to subscribe to tags, too?
* Features
   * User registration with email confirmation.
   * Subscribe/unsubscribe functionality.
   * Per-user quiet/silence/block lists.
      * Quiet: unfollow this user and don’t show their posts in the local/federated firehose feeds. Still show posts of theirs that highlight me, appear in a thread I watch, or are repeated by people I follow.
      * Silence: unfollow this user and don’t show their posts in the local/federated firehose feeds, but still deliver posts of theirs that highlight me, appear in a thread I watch, or are repeated by people I follow to my client with a ‘silenced’ hint that 
  * Per-user profile info.
  * Set user info.
  * Query other user’s info.
  * Subscribe to feeds, people, or groups.
  * Query feeds, people, groups, tags.
  * Data expiration (a year?)
  * Ability to do server-level quiet, silence, or block of individuals or servers.
  * SSL-capable
* Design
   * List of server-level quiets, silences, and blocks is publicly queryable.
   * Delivery scopes:
      * Public: shows up in local and federated timelines, plus to followers and anyone who sees a repeat.
      * Local+: shows up in local instance timeline but NOT federated timelines, and to followers and those who see repeats
      * Unlisted: only shown to those who directly follow or see a repeat
      * Followers-only: implemented in Mastodon, but not sure how I feel about this… supposedly the same as unlisted, plus can’t be repeated to others or accidentally leaked via hashtags… in practice, not well-supported and only guarantees that the post is unlisted.
      * Whisper: Not guaranteed to be private, especially when passing between instances, but is less-public and useful for a quick “here’s my email/phone/etc info to follow up”.
      * Group: deliver only to a specific group.

##### 0.3 Preparation for GUI client development
* Discussion
   * Flags, tags, categories, taxonomies - which are useful, and how should they be defined so as to be as useful as possible?
* Features
   * Content Warnings
   * NSFW flags
   * Media (uploads and linking)
* Design
   * Differentiation between sources so that clients can tell them apart
      * output-only (e.g. public feed)
      * entity/person (will receive replies)
      * Would prevent clients from attempting to reply to things that are feeds from web sites and not other ActivityPub entities (i.e. a person who will see their reply), reducing user confusion.
      * Feeds are auto-generated from RSS/Atom sources (or ostatus), allowing users subscribe to e.g. a random blog without trouble.
      * Hathi-server should return an error to clients that attempt to direct message or otherwise interact with a feed besides subscribing, unsubscribing, or mentioning.
     * Issue tokens for clients so we don’t have the password merry-go-round

##### 0.5 Getting ready to stabilize client API)
* Features
  * Multi-factor Auth: offer TOTP and FIDO as options
  * Can be configured to provide VERY BASIC user and post pages in the absence of Hathi-server or some alternative.

##### 0.7 Test in the wild; last major changes 1.0
* Features
  * User import/export feature
    * MUST include account info (exported-from UUID, avatar, description, etc.), following list, quiet/silence/block lists, and export timestamp.
    * MAY include the user’s post history and/or other settings.

##### 1.0 Stable framework and server API

##### Future: Maybe / Someday / I-Want-a-Unicorn-Dammit
* Features
  * Ostatus bridge to allow federation with ostatus-based server instances
  * Automated abuse detection
  * Some way to self-certify account migrations and alternate accounts via GPG or some other common credential.
  * A mode that prioritizes content based on per-user trust metrics.
  * General-purpose propagation layer protocol for automatic, secure encryption